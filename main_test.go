package main

import "testing"

func TestWithSampledInput(t *testing.T) {
	RAW_LIST_OF_ESSAYS = "endg-urls-2"
	topCounted := fetchTopCountedWords(DEFAULT_THREAD_LIMIT)
	expectedWords := map[string]int{
		"error":    2572,
		"many":     1286,
		"process":  1286,
		"request":  1286,
		"requests": 1286,
		"this":     1286,
	}
	for k, v := range expectedWords {
		if count, exists := topCounted[k]; !exists || count != v {
			t.Logf("test with sample input failed: key [%v] expected [%v] actual [%v]", k, v, count)
			t.Fail()
		}
	}
}

func TestWithSampledInputAndTypoLine(t *testing.T) {
	RAW_LIST_OF_ESSAYS = "endg-urls-3"
	topCounted := fetchTopCountedWords(DEFAULT_THREAD_LIMIT)
	expectedWords := map[string]int{
		"error":    38,
		"many":     19,
		"process":  19,
		"request":  19,
		"requests": 19,
		"this":     19,
	}
	for k, v := range expectedWords {
		if count, exists := topCounted[k]; !exists || count != v {
			t.Logf("test with sample input failed: key [%v] expected [%v] actual [%v]", k, v, count)
			t.Fail()
		}
	}
}
