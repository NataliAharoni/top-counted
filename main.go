package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"unicode"
)

const (
	DEFAULT_THREAD_LIMIT     = 20
	DEFAULT_AMOUNT_TOP_WORDS = 10
	HTTP_RETRIES             = 3
)

var (
	RAW_BANK_LINK      = "https://raw.githubusercontent.com/dwyl/english-words/master/words.txt"
	RAW_LIST_OF_ESSAYS = "endg-urls"

	bankMutex, errorsMutex sync.Mutex
	essaysList             []string
	centralBank            map[string]int
	erroredArticles        map[string]string
)

type WordCount struct {
	word  string
	count int
}

func init() {
	bankMutex = sync.Mutex{}
	errorsMutex = sync.Mutex{}
	essaysList = []string{}
	centralBank = map[string]int{}
	erroredArticles = map[string]string{}
}

func main() {
	threadLimit := DEFAULT_THREAD_LIMIT
	if len(os.Args) > 1 {
		arg := os.Args[1]
		if val, err := strconv.Atoi(arg); err == nil {
			threadLimit = val
		} else {
			fmt.Printf("Warning: Non-numeric argument provided for rate limit value, using default thread limit [%v]\n", DEFAULT_THREAD_LIMIT)
		}
	}
	fmt.Printf("main process for top counted words is running... rate limit [%v]\n", threadLimit)
	topWords := fetchTopCountedWords(threadLimit)
	jsonBytes, err := json.MarshalIndent(topWords, "", "	")
	if err != nil {
		panic(fmt.Sprintf("[ERROR] Could not marshal top words to json: %v\n", err.Error()))
	}
	fmt.Println(string(jsonBytes))

	handleErrors()
	fmt.Println("main process for top counted words is done...")
}

func handleErrors() {
	if len(erroredArticles) > 0 {
		fmt.Println("\n-----")
		fmt.Println("[ERROR] Could not parse the following articles: ")
		for article, errMsg := range erroredArticles {
			fmt.Printf("\nlink: %v\nreason: %v\n", article, errMsg)
		}
		fmt.Println("-----")
	}
}

func prep() {
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		loadListOfEssays()
	}()
	go func() {
		defer wg.Done()
		loadAndFilterBank()
	}()
	wg.Wait()
}

func fetchTopCountedWords(threadLimit int) map[string]int {
	prep()
	semaphore := make(chan struct{}, threadLimit)
	wg := sync.WaitGroup{}
	for _, link := range essaysList {
		wg.Add(1)
		semaphore <- struct{}{}
		go func(link string) {
			defer wg.Done()
			scanArticle(link)
			<-semaphore
		}(link)
	}
	wg.Wait()

	topWords := fetchTopWords(DEFAULT_AMOUNT_TOP_WORDS)
	return topWords
}

func loadListOfEssays() {
	fmt.Println("loading list of essays to cached list")
	bytes, err := os.ReadFile(RAW_LIST_OF_ESSAYS)
	if err != nil {
		panic(fmt.Sprintf("[ERROR] Could not open file %s: %v\n", RAW_LIST_OF_ESSAYS, err.Error()))
	}
	essaysList = strings.Split(string(bytes), "\n")
	fmt.Println("load list of essays to cached list done.")
}

func getRemoteDataWithRetries(url string) (resp *http.Response, err error) {
	for i := 0; i <= HTTP_RETRIES; i++ {
		resp, err = http.Get(url)
		if err != nil {
			err = fmt.Errorf("[ERROR] Could not read remote %s: %v\n", url, err.Error())
			fmt.Println(err.Error())
		} else {
			if (resp.StatusCode < 200 || resp.StatusCode >= 300) && resp.StatusCode != 999 {
				err = fmt.Errorf("[ERROR] Status code is indicated issue to fetch data from remote: %v\n", resp.StatusCode)
				fmt.Println(err.Error())
			} else {
				return resp, nil
			}
		}
		fmt.Printf("attempt [%v]\n", i)
	}
	return nil, err
}

func loadAndFilterBank() {
	fmt.Println("loading and filtering the central words bank...")
	response, err := getRemoteDataWithRetries(RAW_BANK_LINK)
	if err != nil {
		panic(fmt.Sprintf("[ERROR] Could not read remote file %s\n", RAW_BANK_LINK))
	}
	defer response.Body.Close()
	scanner := bufio.NewScanner(response.Body)
	for scanner.Scan() {
		line := scanner.Text()
		words := strings.Split(line, " ")
		for _, word := range words {
			if passBasicFilter(word) {
				centralBank[word] = 0
			}
		}
	}
	fmt.Println("central words bank loading and filtering done.")
}

func scanArticle(link string) {
	fmt.Printf("scanning article [%v]\n", link)
	localBank := map[string]int{}
	response, err := getRemoteDataWithRetries(link)
	if err != nil {
		errorsMutex.Lock()
		erroredArticles[link] = err.Error()
		errorsMutex.Unlock()
		return
	}
	defer response.Body.Close()
	scanner := bufio.NewScanner(response.Body)
	for scanner.Scan() {
		line := scanner.Text()
		words := strings.Split(line, " ")
		for _, word := range words {
			if passBasicFilter(word) {
				bankMutex.Lock()
				_, exists := centralBank[word]
				bankMutex.Unlock()
				if exists {
					localBank[word]++
				}
			}
		}
	}
	fmt.Printf("scanning article [%v] done.\n", link)
	syncLocalBankToCentralBank(link, localBank)
}

func syncLocalBankToCentralBank(link string, localBank map[string]int) {
	fmt.Printf("syncing local bank of article [%v] to central bank\n", link)
	bankMutex.Lock()
	for k, v := range localBank {
		centralBank[k] += v
	}
	bankMutex.Unlock()
	fmt.Printf("syncing local bank of article [%v] to central bank done.\n", link)
}

func fetchTopWords(amount int) map[string]int {
	fmt.Printf("fetching top counted words, requested amount [%v]\n", amount)
	topCountedWords := make([]WordCount, amount)

	for word, count := range centralBank {
		for i := 0; i < amount; i++ {
			if count >= topCountedWords[i].count {
				copy(topCountedWords[i+1:], topCountedWords[i:])
				topCountedWords[i] = WordCount{word, count}
				break
			}
		}
	}
	result := map[string]int{}
	for i := range topCountedWords {
		result[topCountedWords[i].word] = topCountedWords[i].count
	}
	fmt.Printf("fetching top counted words, requested amount [%v] done\n", amount)
	return result
}

// Utils

func passBasicFilter(word string) bool {
	return len(word) > 2 && isAlphabetic(word)
}

func isAlphabetic(s string) bool {
	for _, c := range s {
		if !unicode.IsLetter(c) {
			return false
		}
	}
	return true
}
